<?php
return array(
	/* 前台设置  */
	'WATER_QUALITY' => array(
		array("key"=>'1' ,"value" =>'自来水'),
		array("key"=>'2' ,"value" =>'井水'),
		array("key"=>'3' ,"value" =>'其他水')
	),
	'CUSTOMER_TYPE'=>array(
		array("key"=>'1' ,"value" =>'个人客户'),
		array("key"=>'2' ,"value" =>'单位客户')
	),
	'CUSTOMER_GRADE'=>array(
		array("key"=>'1' ,"value" =>'普通客户'),
		array("key"=>'2' ,"value" =>'优质客户'),
		array("key"=>'3' ,"value" =>'VIP客户')
	),
	'CITY'=>array(
		array("key"=>'100' ,"value" =>'东阳'),
		array("key"=>'101' ,"value" =>'义乌')
	)
);