<?php
namespace Admin\Model;
use Think\Model;
class CustomerModel extends CommonModel{

//	public function getCustomerInfo($custid)
//	{
//
//
//	}

	public function getCustomerProductFittingInfo($custid,$prodid)
	{
		$cust_prod = M("custprod");
		$cust_prod_data = $cust_prod->where(array("custid"=>$custid,"prodid"=>$prodid))->find();
		if($cust_prod_data)
		{
			$buyfitid = $cust_prod_data['buyfitid'];
			$fields = array("ft.fitid","ft.fitname","ft.defaultcycle","cycle");
			$buyfit_db = M("buyfit");
			$data = $buyfit_db ->field($fields)->join('__FITTING__ ft on ft.fitid= __BUYFIT__.fitid')->where(array("buyfitid"=>$buyfitid))->select();
		}else{
			$field = array('ft.fitid','`fitname`','cycle','cycle as custcycle');
			$order = '`fitname` DESC';
			$where =array('prodid'=>$prodid);
			$prod_fitting_db = M("product_fitting");
			$data = $prod_fitting_db->field($field)->join('__FITTING__ ft ON ft.fitid = __PRODUCT_FITTING__.fitid')->where($where)->order($order)->select();
		}
		return $data;
	}


	public function addProduct($custProdData,$fitdata)
	{
		try{
			$this->startTrans();
			$this->add($custProdData);
			if($fitdata == null)
			{
				$prodfit_db = M("product_fitting");
				$prodfitdata = $prodfit_db->field(array("fitid","cycle"))->where(array("prodid"=>$custProdData["prodid"]))->select();
				$custfit_db = M("custfit");
				foreach($prodfitdata as $prodfit)
				{
					$prodfit["custid"]=$custProdData["custid"];
					$prodfit["prodid"]=$custProdData["prodid"];
					$custfit_db->add($prodfit);
				}
			}else{
				foreach($fitdata as $prodfit)
				{
					$prodfit["custid"]=$custProdData["custid"];
					$prodfit["prodid"]=$custProdData["prodid"];
					$custfit_db->add($prodfit);
				}

			}
			$this->commit();
			return true;
		}catch (PDOException $ex)
		{
			$this->rollback();
			return null;
		}
	}







}