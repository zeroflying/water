<?php
namespace Admin\Model;
use Think\Model;
class BrandModel extends CommonModel{
	protected $tableName = 'brand';
	protected $pk        = 'brandid';

	public function getBrandList()
	{
		$field = array('brandid','`brandname`');
		$order='`brandname` DESC';
		$data = $this->field($field)->order($order)->select();
		return $data;
	}

	public function clearCatche()
	{
		S("brandSelect",null);
	}
}