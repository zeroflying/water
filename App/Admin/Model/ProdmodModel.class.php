<?php
namespace Admin\Model;
use Think\Model;
class  ProdmodModel extends  CommonModel{
	protected $tableName = 'prodmod';
	protected $pk        = 'modelid';
//	protected $_link = array(
//		"fitting"=>array(
//			'mapping_type' => self::MANY_TO_MANY,
//            'foreign_key' =>  'modelid',   //主表在中间表中的字段名称
//    		'relation_foreign_key' =>  'fitid'   //关联表(副表)在中间表中的字段名称(外键)
//			//'relation_table'=>'__productmod_fitting__'
//		)
//	);


	public function getProductModelList()
	{
		trace("testlog");
		$field = array('modelid','modelname');
		$order = '`modelid` ASC';
		$data = $this->field($field)->order($order)->select();
		trace($this->getLastSql());
		return $data;
	}

	public function getFittingsByModel($modelid)
	{
		$db = M('prodmod_fitting');
		$data = $db->join('__FITTING__ ON __PRODMOD_FITTING__.fitid=__FITTING__.fitid')
		->where('modelid='.$modelid)->select();
		return $data;
	}

}