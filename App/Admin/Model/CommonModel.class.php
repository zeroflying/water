<?php
namespace Admin\Model;
use Think\Model;
class CommonModel extends Model
{
	protected function _before_insert(&$data,$options) {
		$data["createtime"] = session("createtime");
		$data["createid"] = session("userid");
		$data["updatetime"] = session("updatetime");
		$data["updateid"] = session("userid");
		return true;
	}

	protected function _before_update(&$data,$options) {
		$data["updatetime"] = S("updatetime");
		$data["updateid"] = S("userid");
		return true;
	}
}