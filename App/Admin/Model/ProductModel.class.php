<?php
namespace Admin\Model;

use Think\Model;
class ProductModel extends CommonModel{
	protected $tableName = 'product';
	protected $pk        = 'prodid';

//	//获取栏目列表
//	public function getTree(){
//		$field = array('prodid','`prodname`','brandname','type','modelname','brandid','modelid','persnum','waterqual');
//		$order = '`prodid` ASC';
//		$data = $this->field($field)->order($order)->select();
//		trace();
//		return $data;
//	}

	public function getProductList()
	{
		$field = array('prodid','`prodname`','brandname','type','modelname','br.brandid','pd.modelid','persnum','waterqual');
		$order = '`prodname` DESC';
		$data = $this->field($field)->join('__PRODMOD__ pd ON pd.modelid=__PRODUCT__.modelid')->join('__BRAND__ br ON br.brandid=__PRODUCT__.brandid')->order($order)->select();
		return $data;
	}

	public function addProduct($data,$fitdata)
	{
		try{
			$this->startTrans();
			//product table
			$prod_id = $this->add($data);
			//product fitting
			$prod_fitting_db = M("product_fitting");

			$fitting_db = M("fitting");
			trace($fitdata,"ERR");

			foreach ($fitdata as $fit)
			{
				if(!isset($fit['fitid']))
				{
					$fit['fitid']= $fitting_db.add(array("fitname"=>$fit.fitname,"defaultcycle"=>$fit.cycle,"fittype"=>1));
					trace($fitting_db->getLastSql(),"ERR");
				}
				$fit['prodid'] =$prod_id;
				$prod_fitting_db->add($fit);
				trace($prod_fitting_db->getLastSql(),"ERR");
			}
			$this->commit();
			return $prod_id;
		}catch (PDOException $ex)
		{
			$this->rollback();
			return null;
		}
	}

	public function getProduct($id)
	{
		$field = array('prodid','`prodname`','brandname','type','modelname','br.brandid','pd.modelid','persnum','waterqual');
		$order = '`prodname` DESC';
		$where = array('prodid'=>$id);
		$data = $this->field($field)->join('__PRODMOD__ pd ON pd.modelid=__PRODUCT__.modelid')->join('__BRAND__ br ON br.brandid=__PRODUCT__.brandid')->where($where)->order($order)->find();
		return $data;
	}

	public function getRelatedFitting($id)
	{
		$field = array('ft.fitid','`fitname`','cycle');
		$order = '`fitname` DESC';
		$where =array('prodid'=>$id);
		$prod_fitting_db = M("product_fitting");
		$data = $prod_fitting_db->field($field)->join('__FITTING__ ft ON ft.fitid = __PRODUCT_FITTING__.fitid')->where($where)->order($order)->select();
		return $data;
	}



	public function saveProduct($id,$productdata,$fitdata)
	{
		try{
			$this->startTrans();
			//product table
			$this->where(array('prodid'=>$id))->save($productdata);
			//product fitting
			$prod_fitting_db = M("product_fitting");

			$fitting_db = M("fitting");
			trace($fitdata,"ERR");

			foreach ($fitdata as $fit)
			{
				if(!isset($fit['fitid']) || empty($fit['fitid']))
				{
					$fit['fitid']= $fitting_db->add(array("fitname"=>$fit['fitname'],"defaultcycle"=>$fit['cycle'],"fittype"=>1));
					trace($fitting_db->getLastSql(),"ERR");
				}
				$fit['prodid'] =$id;
				$isProdFitExist = $prod_fitting_db->where(array('prodid'=>$fit['prodid'],'fitid'=>$fit['fitid']))->find();
				trace($isProdFitExist);
				if($isProdFitExist)
				{
					trace("save data");
					$prod_fitting_db->where(array('prodid'=>$fit['prodid'],'fitid'=>$fit['fitid']))->save($fit);
					trace($prod_fitting_db->getLastSql());
				}else{
					$prod_fitting_db->add($fit);
				}

				trace($prod_fitting_db->getLastSql(),"ERR");
			}
			$this->commit();
			return true;
		}catch (PDOException $ex)
		{
			$this->rollback();
			return false;
		}
	}

	public function deleteProduct($id)
	{
		try{
			$this->startTrans();
			//product table
			$this->where(array('prodid'=>$id))->delete();
			trace($this->getLastSql());
			//product fitting
			$prod_fitting_db = M("product_fitting");

			$prod_fitting_db->where(array('prodid'=>$id))->delete();
			trace($this->getLastSql());
			$this->commit();
			return true;
		}catch (PDOException $ex)
		{
			$this->rollback();
			return false;
		}
	}
	public function getProductTypesByBrand($brandid)
	{
		$data = $this->field(array("type","type as value"))->distinct(true)->where(array("brandid"=>$brandid))->select();
		trace($this->getLastSql());
		return $data;
	}



	public function clearCatche()
	{
		S("productFittingList",null);
		S("productModeSelect",null);
		S("brandSelect",null);
		S("product_list",null);

	}

	public function getProductName($brandid,$prodtype)
	{
		$data = $this->field(array("prodid","prodname"))->where(array("brandid"=>$brandid,"type"=>$prodtype))->find();
		trace($this->getLastSql());
		return $data;
	}
//	public function getFittingList($modeid)
//	{
//		$model=M('prodmodefit');
//		$data = $model->join("fittings on fittings.fitid=prodmodefit.fitid")
//		       ->select();
////        $members=$model->table(' m,fittings f')
////              ->where('m.fitid=f.fitid')
////              ->field('f.fitid,f.fitname,f.defaultcycle,f.fittype')
////              ->order('f.abbrname')
////              ->select();
//        return $data;
//	}
}