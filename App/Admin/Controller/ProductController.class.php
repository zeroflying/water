<?php
namespace Admin\Controller;
use Think\Model;

use Admin\Controller\CommonController;

class ProductController extends CommonController
{
	function productList(){
		if(IS_POST){
			if(S('product_list')){
    			$data = S('product_list');
    		}else{
    			$product_db = D('Product');
    			$data = $product_db->getProductList();
    			S('product_list', $data);
    		}
    		$this->ajaxReturn($data);
		}else{
			$menu_db = D('Menu');
			$currentpos = $menu_db->currentPos(I('get.menuid'));  //栏目位置
			$treegrid = array(
		        'options'       => array(
    				'title'     => $currentpos,
    				'url'       => U('Product/productList', array('grid'=>'treegrid')),
    				'idField'   => 'prodid',
    				'toolbar'   => 'product_list_treegrid_toolbar',
    			),
		        'fields' => array(
		        	'产品名称'    => array('field'=>'prodname','width'=>20,'align'=>'center'),
		        	'品牌名称'  => array('field'=>'brandname','width'=>25,'align'=>'center'),
		        	'型号' => array('field'=>'type','width'=>30),
		        	'机型' => array('field'=>'modelname','width'=>30),
		        	'使用人数'    => array('field'=>'persnum','width'=>20),
    				'适用水质'    => array('field'=>'waterqual','width'=>30,'formatter'=>'productWaterQualityFormatter'),
		        	'管理操作' => array('field'=>'prodid','width'=>50,'align'=>'center','formatter'=>'productListOperateFormatter'),
    			)
		    );
		    $this->assign('treegrid', $treegrid);
			$this->display('product_list');
		}
	}



	public function productAdd(){
		if(IS_POST){
			$data = I('post.info');
			$fitdata = array();
			for($i=0; $i<count($_REQUEST['fitname']); $i++)
			{
				 array_push($fitdata,
				    array(
				  		"fitname"=> $_REQUEST['fitname'][$i],
				  		"cycle" => $_REQUEST['defaultcycle'][$i],
				    	"fitid" => $_REQUEST['fitid'][$i]
				    ));
			}
			$product_db = D('Product');
			$id = $product_db->addProduct($data,$fitdata);
    		if($id){
    		//	$product_db->clearCatche();
    			$this->success('添加成功');
    		}else {
    			$this->error('添加失败');
    		}
		}else{
			$this->assign('waterQualityList', json_encode(dict('WATER_QUALITY','BusinessDict')));
			$this->assign("brandlist",self::getBrandList());
			$this->assign('productModelList',self::getProductModelList());
			$this->display('product_add');
		}
	}


	public function productEdit($id)
	{
		$product_db = D('Product');
		if(IS_POST){
			$productdata = I('post.info');
			$fitdata = array();
			for($i=0; $i<count($_REQUEST['fitname']); $i++)
			{
				 array_push($fitdata,
				    array(
				  		"fitname"=> $_REQUEST['fitname'][$i],
				  		"cycle" => $_REQUEST['cycle'][$i],
				    	"fitid" => $_REQUEST['fitid'][$i]
				    ));
			}
			$res = $product_db->saveProduct($id,$productdata,$fitdata);
    		if($res){
    			$product_db->clearCatche();
    			$this->success('操作成功');
    		}else {
    			$this->error('操作失败');
    		}
		}else{
			$info = $product_db->getProduct($id);
			$fitinfo = $product_db->getRelatedFitting($id);
			$this->assign('info', $info);
			$this->assign('fitinfo', $fitinfo);
			$this->assign("brandlist",self::getBrandList());
			$this->assign('productModelList',self::getProductModelList());
			$this->assign('waterQualityList', json_encode(dict('WATER_QUALITY','BusinessDict')));
			$this->display('product_edit');
		}
	}


	public function productDelete($id)
	{
		if($id)
		{
			$product_db = D('Product');
			$res = $product_db->deleteProduct($id);
			if($res)
			{
				$product_db->clearCatche();
				$this->success("删除成功.");
			}else{
				$this->error("删除失败.");
			}

		}else{
			$this->error('没有对应的产品需要删除!');
		}

	}
	public function getBrandList()
	{
		if(S('brandSelect')){
    		$data = S('brandSelect');
    	}else {
    		$product_db = D('Brand');
    		$data = json_encode($product_db->getBrandList());
    		S('brandSelect', $data);
    	}
    	return $data;
	}

	public function getProductModelList()
	{
		if(S('productModeSelect')){
    		$data = S('productModeSelect');
    	}else {
    		$product_db = D('Prodmod');
    		$data = json_encode($product_db->getProductModelList());
    		S('productModeSelect', $data);
    	}
    	return $data;
	}


	public function getFittingList($modelid)
	{
		if(S('productFittingList'))
		{
			$data = S('productFittingList');
		}else{
			$db = D('Prodmod');
	    	$data = json_encode($db->getFittingsByModel($modelid));
	    	S('productFittingList', $data);
		}
		$this->ajaxReturn($data,"json");
	}


	public function getProductName($brandid,$prodtype)
	{
		$db = D('Product');
    	$data = json_encode($db->getProductName($brandid,$prodtype));
		$this->ajaxReturn($data,"json");
	}
}
