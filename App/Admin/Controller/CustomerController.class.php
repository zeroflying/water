<?php
namespace Admin\Controller;
use Admin\Controller\CommonController;
class  CustomerController extends CommonController{
	function customerList(){
		if(IS_POST){
			if(S('customer_list')){
    			$data = S('customer_list');
    		}else{
    			$product_db = D('Product');
    			$data = $product_db->getProductList();
    			S('customer_list', $data);
    		}
    		$this->ajaxReturn($data);
		}else{
			$menu_db = D('Menu');
			$currentpos = $menu_db->currentPos(I('get.menuid'));  //栏目位置
			$treegrid = array(
		        'options'       => array(
    				'title'     => $currentpos,
    				'url'       => U('Customer/customerList', array('grid'=>'treegrid')),
    				'idField'   => 'prodid',
    				'toolbar'   => 'customer_list_treegrid_toolbar',
    			),
		        'fields' => array(
		        	'产品名称'    => array('field'=>'prodname','width'=>20,'align'=>'center'),
		        	'品牌名称'  => array('field'=>'brandname','width'=>25,'align'=>'center'),
		        	'型号' => array('field'=>'type','width'=>30),
		        	'机型' => array('field'=>'modelname','width'=>30),
		        	'使用人数'    => array('field'=>'persnum','width'=>20),
    				'适用水质'    => array('field'=>'waterqual','width'=>30,'formatter'=>'customerWaterQualityFormatter'),
		        	'管理操作' => array('field'=>'prodid','width'=>50,'align'=>'center','formatter'=>'customerListOperateFormatter'),
    			)
		    );
		    $this->assign('treegrid', $treegrid);
			$this->display('customer_list');
		}
	}

	public function customerAdd(){
		if(IS_POST){
			$data = I('post.info');
			$customer_db = D('Customer');
			$id = $customer_db->add($data);
    		if($id){
    		//	$product_db->clearCatche();
    		    $msg = array("msg"=>"添加成功","id"=>$id);
    			$this->success($msg);
    		}else {
    			$this->error('添加失败');
    		}
		}else{
			$this->assign('CustomerTypeList', json_encode(dict('CUSTOMER_TYPE','BusinessDict')));
			$this->assign('CustomerGradeList', json_encode(dict('CUSTOMER_GRADE','BusinessDict')));
			$this->assign('CityList', json_encode(dict('CITY','BusinessDict')));
			$this->display('customer_add');
		}
	}

	public function customerAddProduct()
	{
		$customer_db = D("Customer");
		if(IS_POST)
		{
			$data = I('post.info');
			$customer_db->addProduct($data);
		}else{
			$custid = $_REQUEST["custid"];
			$info = $customer_db->where(array('custid'=>$custid))->find();
			$brandlist = R("Product/getBrandList");
			$this->assign('cust', $info);
			$this->assign('brandlist', $brandlist);
			$this->display('customer_add_product');
		}
	}

	public function getProductTypesByBrand($brandid)
	{
    	$product_db = D('Product');
    	$data = json_encode($product_db->getProductTypesByBrand($brandid));
    	$this->ajaxReturn($data,"json");
	}

	public function getCustomerProductFittingInfo($custid,$prodid)
	{
		$cycleCache = S("CustomerFilterCycleData");
		trace("cache: ");
		trace($cycleCache);
		trace($cycleCache[$custid+":"+$prodid]);
		if( $cycleCache!= null && $cycleCache[$custid+""+$prodid] != null )
		{
			$data = $cycleCache[$custid+$prodid];
		}else{
			$customer_db = D("Customer");
			$data = $customer_db->getCustomerProductFittingInfo($custid,$prodid);
			trace("customer fitting: ");
			trace($data);
		}

		$this->assign("fitlist",$data);
		$this->assign("custid",$custid);
		$this->assign("prodid",$prodid);
		$this->display("filter_change");
		//$this->ajaxReturn(json_encode($data),"json");
	}

	public function storeCustomerFilterCycle()
	{
		if(IS_POST){
			S("CustomerFilterCycleData", null);
			$data = I('post.info');
			$fitdata = array();
			for($i=0; $i<count($_REQUEST['fitid']); $i++)
			{
				 array_push($fitdata,
				    array(
				  		"fitname"=> $_REQUEST['fitname'][$i],
				  		"custcycle" => $_REQUEST['custcycle'][$i],
				    	"cycle" => $_REQUEST['cycle'][$i],
				    	"fitid" => $_REQUEST['fitid'][$i]
				    ));
			}
			trace($fitdata);
			$cacheData = array(($data["custid"]+":"+$data["prodid"])=>$fitdata);
			S("CustomerFilterCycleData", $cacheData);
		}
	}



}