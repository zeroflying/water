$(function(){
	$.formValidator.initConfig({formID:"customer_add_product_filter_change_dialog_form",onError:function(msg){$.messager.alert('错误提示', msg, 'error');},onSuccess:filterChangeFormSubmit,submitAfterAjaxPrompt:'有数据正在异步验证，请稍等...',inIframe:true});
	$("#customer_add_product_filter_change_dialog_form_add_filter").click(function(){addFilter()});

})

function deleteFilter(obj)
{
	$(obj).parent().parent().remove();
}

function addFilter()
{
	var content = '<tr><td width="160"><input type="text" name="fitname[]" class="shortTextField"></td>'+
	'<td><input type="text" class="textField" disabled="disabled" value="没有默认值"></td>'+
	'<td width="250px"><input type="text" class="textField" name="custcycle[]"  maxlength="5"/>'+
	'<span style="padding-right:6px"></span><a class="easyui-linkbutton" data-options="plain:true,iconCls:\'icons-other-delete\'" onclick="deleteFilter(this)"></a></td></tr><tr></tr>';				
	$("#customer_add_product_filter_change_dialog_filter_content").append(content);
	$("#customer_add_product_filter_change_dialog_filter_content a").linkbutton();
}

function filterChangeFormSubmit()
{
	$.post(U('Customer/storeCustomerFilterCycle'), $("#customer_add_product_filter_change_dialog_form").serialize(), function(res){
		$('#customer_add_product_filter_change_dialog').dialog('close');});
}

//<script type="text/javascript">
//$(function(){
//	$.formValidator.initConfig({formID:"customer_add_dialog_form",onError:function(msg){$.messager.alert('错误提示', msg, 'error');},onSuccess:customerAddDialogFormSubmit,submitAfterAjaxPrompt:'有数据正在异步验证，请稍等...',inIframe:true});
//})
//function customerAddDialogFormSubmit(){
//	$.post('<{:U('Customer/customerAdd')}>', $("#customer_add_dialog_form").serialize(), function(res){
//		console.log(res);
//		if(!res.status){
//			$.messager.alert('提示信息', res.info, 'error');
//		}else{
//			$('#customer_add_dialog').dialog('close');
//			$.messager.confirm('操作提示', res.info["msg"]+"\n 你需要给客户添加产品吗？",function(data){
//				if(data)
//				{
//					customerAddProduct(res.info["id"]);
//				}else{
//					customerListRefresh();
//				}		
//			});
//		}
//	})
//}
//
//function deleteFilter(obj)
//{
//	$(obj).parent("tr").remove();
//}
//
//function addFilter()
//{
//	var content ='<tr><td class="td-lable"><input type="text" name="fitname[]" width="150px"></td>'+
//		'<td><input type="text" name="cycle[]" value="无默认周期"  disabled="disabled"></td><td><input type="text" name="custcycle[]">'+
//		'<span class="deptcbk" id="err"></span><a class="easyui-linkbutton" data-options="plain:true,iconCls:\'icons-other-delete\'" onclick="deleteFilter(this)"></a> </td></tr><tr></tr>';
//	$("#filterContent").append(content);
//	$("#filterContent td:last a").linkbutton();
//}
//</script>