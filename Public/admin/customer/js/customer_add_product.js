
$(function(){
	$.formValidator.initConfig({formID:"customer_add_product_form",onError:function(msg){$.messager.alert('错误提示', msg, 'error');},onSuccess:customerAddProductFormSubmit,submitAfterAjaxPrompt:'有数据正在异步验证，请稍等...',inIframe:true});
	$("#customer_add_product_form_submit").click(function(){$("#customer_add_product_form").submit()});
})
function customerAddProductFormSubmit(){
	$.post(U('Customer/customerAddProduct'), $("#customer_add_product_form").serialize(),function(res)
		{
			if(!res.status){
				$.messager.alert('提示信息', res.info, 'error');
			}else{
				$.messager.alert('提示信息', res.info, 'success');
			}
		});
}

function productBrandChange(val){
	var params = {"brandid":val.brandid};
	$.get(U("Customer/getProductTypesByBrand"),params,function(data){
		$("#product_add_dialog_form_type").combobox("loadData",eval(data));
	});
}

function productTypeChange(val){
	var brandid = $("#product_add_dialog_form_brandid").combobox("getValue");
	var params = {"brandid":brandid,"prodtype":val.type};
	var url = U("Product/getProductName");
	$.get(url,params,function(data){
		var json = eval("("+data+")");
		$("#product_add_dialog_form_prodid").val(json.prodid);
		$("#product_add_dialog_form_prodname").val(json.prodname);
	});
}

function changeFilterCycle()
{
	var prodid = $("#product_add_dialog_form_prodid").val();
	if(prodid == "undefined" || prodid=="")
	{
		alert("请选择产品!");
		return;
	}
	var url = U('Customer/getCustomerProductFittingInfo');
	var params = "&custid="+$("#custid").val()+"&prodid="+prodid;
	$('#customer_add_product_filter_change_dialog').dialog({href:url+params})
	$('#customer_add_product_filter_change_dialog').dialog('open');
}


